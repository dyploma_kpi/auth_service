from contextlib import asynccontextmanager
from src.config import settings

from fastapi import FastAPI, status
from src.service import AuthService, auth_service

from src.models.schems import (UserInfo, LoginUser, RegisterUser, TokenInfo)


@asynccontextmanager
async def lifespan(app: FastAPI):
    global auth_service
    auth_service = auth_service
    yield
    auth_service = None
    
    
app = FastAPI(lifespan=lifespan, openapi_prefix=settings.api_prefix)


@app.post("/register", status_code=status.HTTP_201_CREATED)
async def register(user: RegisterUser):
    if await auth_service.register(user):
        return {
            "message": "Refistered Succesfully",
        }
    
@app.post("/login", response_model=TokenInfo, status_code=status.HTTP_202_ACCEPTED)
async def login(login_user: LoginUser):
    return await auth_service.login(login_user)
    
@app.post("/verify", response_model=UserInfo, status_code=status.HTTP_202_ACCEPTED)
async def verify(token_info: TokenInfo):
    return await auth_service.verify(token_info)
    
@app.get("/validate", status_code=status.HTTP_202_ACCEPTED)
def validate():
    return {
        "message": "Hello index!",
    }
