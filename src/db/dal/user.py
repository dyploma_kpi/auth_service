import asyncio

from sqlalchemy import select
from sqlalchemy.engine import Result
from sqlalchemy.orm import joinedload, selectinload
from sqlalchemy.ext.asyncio import AsyncSession

from src.models.dal import (
    User,
)
from src.db import DatabaseHelper, db_helper

class UserDAL():
    
    def __init__(self, db_helper: DatabaseHelper) -> None:
        self._db_helper = db_helper
        

    async def create_user(self, email: str, phone: str, hash_pass: str) -> int | None:
        async with self._db_helper.session_factory() as session:
            try:
                user = User(email=email, phone=phone, hashed_password=hash_pass)
                session.add(user)
                await session.commit()
                await session.refresh(user)  # Refresh to get the generated ID
                return user.id
            except Exception as e:
                print(f"Error creating user: {e}")
                return None
            
    async def get_user_by_email(self, email: str) -> User | None:
        async with self._db_helper.session_factory() as session:
            stmt = select(User).where(User.email == email)
            user: User | None = await session.scalar(stmt)
            return user
        
    async def get_user_by_phone(self, phone: str) -> User | None:
        async with self._db_helper.session_factory() as session:
            stmt = select(User).where(User.phone == phone)
            user: User | None = await session.scalar(stmt)
            return user


user_dal = UserDAL(db_helper)
    