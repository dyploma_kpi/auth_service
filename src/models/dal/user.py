from sqlalchemy import String, Boolean, DateTime
import datetime
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column

from src.models.dal.base import Base


class User(Base):
    email: Mapped[str] = mapped_column(String(100), unique=True, nullable=False)
    phone: Mapped[str] = mapped_column(String(13), unique=True, nullable=False)
    hashed_password: Mapped[str] = mapped_column(String(256), nullable=False)
    registrated_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())
    modified_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), onupdate=func.now(), server_default=func.now())
    is_active: Mapped[bool] = mapped_column(Boolean, nullable=False, server_default='0')
