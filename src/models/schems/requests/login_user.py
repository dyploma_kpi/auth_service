from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, EmailStr


class LoginUser(BaseModel):
    email: EmailStr
    password: Annotated[str, MinLen(12), MaxLen(30)]
