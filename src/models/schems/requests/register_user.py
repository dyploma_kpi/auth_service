from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, EmailStr


class RegisterUser(BaseModel):
    email: EmailStr
    phone: Annotated[str, MinLen(13), MaxLen(13)]
    password: Annotated[str, MinLen(12), MaxLen(30)]
    fname: Annotated[str, MinLen(3), MaxLen(50)]
    sname: Annotated[str, MinLen(3), MaxLen(50)]
