from pydantic import BaseModel, EmailStr, Field


class UserInfo(BaseModel):
    id: int = Field(None, ge=1)
    email: EmailStr
