from datetime import datetime, timedelta

import bcrypt
import jwt
from pathlib import Path

from src.config import settings


class AuthHelper():
    def __init__(
        self,
        private_key_path: Path,
        public_key_path: Path,
        algorithm: str,
        expire_minutes: int
    ) -> None:
        self._private_key: str = private_key_path.read_text()
        self._public_key: str = public_key_path.read_text()
        self._algorithm: str = algorithm
        self._expire_minutes: int = expire_minutes
        
    def encode_jwt(self, payload: dict) -> str:
        to_encode = payload.copy()
        now = datetime.now()
        expire = now + timedelta(minutes=self._expire_minutes)
        to_encode.update(
            exp=expire,
            iat=now,
        )
        encoded = jwt.encode(
            to_encode,
            self._private_key,
            algorithm=self._algorithm,
        )
        return encoded


    def decode_jwt(self, token: str | bytes) -> dict:
        decoded = jwt.decode(
            token,
            self._public_key,
            algorithms=[self._algorithm],
        )
        return decoded


    def hash_password(self, password: str) -> bytes:
        salt = bcrypt.gensalt()
        pwd_bytes: bytes = password.encode()
        return bcrypt.hashpw(pwd_bytes, salt)


    def validate_password(self, password: str, hashed_password: bytes) -> bool:
        return bcrypt.checkpw(
            password=password.encode(),
            hashed_password=hashed_password,
        )

auth_helper = AuthHelper(
                settings.auth_jwt.private_key_path, 
                settings.auth_jwt.public_key_path,
                settings.auth_jwt.algorithm,
                settings.auth_jwt.access_token_expire_minutes)
