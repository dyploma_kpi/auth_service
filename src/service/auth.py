from src.db import user_dal, UserDAL
from src.utils import AuthHelper, auth_helper
from src.models.schems import (LoginUser, RegisterUser, TokenInfo, UserInfo)
from src.models.dal import User
from src.config import settings
from fastapi import HTTPException, status
import httpx
from datetime import datetime


class AuthService():
    def __init__(self, user_dal: UserDAL, auth_helper: AuthHelper) -> None:
        self._user_dal: UserDAL = user_dal
        self._auth_hepler: AuthHelper = auth_helper
        
    async def login(self, login_user: LoginUser) -> TokenInfo:
        user: User = await self._user_dal.get_user_by_email(login_user.email)
        if not user:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Incorrect email or password"
            )
        if not self._auth_hepler.validate_password(login_user.password, user.hashed_password.encode('UTF-8')):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Incorrect email or password"
            )
        token: str = self._auth_hepler.encode_jwt({"sub": user.id, "email": user.email})
        return TokenInfo(access_token=token, token_type="Bearer")
    
    async def register(self, user: RegisterUser) -> bool:
        if await self._user_dal.get_user_by_email(user.email):
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="user with that email email exist"
            )
            
        if await self._user_dal.get_user_by_phone(user.phone):
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="user with that phone exist"
            )
            
        hashed_password = self._auth_hepler.hash_password(user.password)
        user_id: int | None =  await self._user_dal.create_user(user.email, user.phone, hashed_password)
        if not user_id:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        
        async with httpx.AsyncClient() as client:
            response = await client.post(
                settings.profile_api_url,
                json={"id": user_id, "first_name": user.fname, "last_name": user.sname}
            )

            if response.status_code != status.HTTP_201_CREATED:
                raise HTTPException(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail="Failed to create user profile in the profile service"
                )
        return True
    
    async def verify(self, token: TokenInfo) -> UserInfo:
        if token.token_type != "Bearer":
            raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail="Bad token"
                )
        try:
            payload: dict[str] = self._auth_hepler.decode_jwt(token.access_token)
        except:
            raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail="Bad token"
                )
        if datetime.fromtimestamp(payload['iat']) > datetime.now():
            raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail="Bad token"
                )
        return UserInfo(id=payload["sub"], email=payload["email"])
        
    
    def validate(self, token: str) -> None:
        pass
    
auth_service = AuthService(user_dal, auth_helper)
